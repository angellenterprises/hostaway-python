from unittest import TestCase


from hostaway.util import read_json


class BaseTestConfig(TestCase):

    json_fixtures = {}

    @classmethod
    def setUpClass(cls):
        cls.json_fixtures = read_json('./tests/fixtures/hostaway_api.json')
