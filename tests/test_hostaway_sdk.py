#!/usr/bin/env python
# coding: utf-8

from testing_config import BaseTestConfig
from unittest.mock import Mock, patch

from datetime import datetime, timedelta

import hostaway
from hostaway.resource.base import Auth

class TestHostawaySDK(BaseTestConfig):
    """TestHostawaySDK."""

    auth: Auth = None

    @patch('hostaway.client.requests.post')
    def setUp(cls, mock_post):
        print('should auth')

        mock_post.return_value = Mock(status_code=200)
        mock_post.return_value.json.return_value = cls.json_fixtures['accessToken']['valid']

        cls.auth = hostaway.Auth.get_token(
            client_id=cls.json_fixtures['api']['id'],
            client_secret=cls.json_fixtures['api']['secret'],
        )
        cls.assertEqual(cls.auth.to_any_object(), cls.json_fixtures['accessToken']['valid'])


    def test_hostaway_construct(cls):
        print('should construct based on access token')
        try:
            # hostaway.api_client_id = cls.auth.id
            hostaway.api_access_token = cls.auth.access_token
            sdk = hostaway.HostawaySdk(
                id=cls.json_fixtures['api']['id'],
                expires_in=cls.auth.expires_in,
                token_type=cls.auth.token_type,
                scope=cls.auth.scope,
            )
        except Exception:
            cls.fail("HostawaySdk() raised Exception unexpectedly!")

    # TEST: get_listing
    @patch('hostaway.client.requests.get')
    def test_hostaway_get_listing(cls, mock_get):
        print('should get listing')
        
        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['listings']

        hostaway.api_access_token = cls.auth.access_token
        sdk = hostaway.HostawaySdk(
            id=cls.json_fixtures['api']['id'],
            expires_in=cls.auth.expires_in,
            token_type=cls.auth.token_type,
            scope=cls.auth.scope,
        )
        response = sdk.get_listings()
        cls.assertNotEqual(response, cls.json_fixtures['listings'])
        print('TESTED: get_listing')

    # TEST: get_calendar
    @patch('hostaway.client.requests.get')
    def test_hostaway_get_calendar(cls, mock_get):
        print('should get calendar')

        start_date = datetime.now()
        end_date = start_date + timedelta(days=365)

        start_date = datetime.strftime(start_date, '%Y-%m-%d')
        end_date = datetime.strftime(end_date, '%Y-%m-%d')

        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['calendar']['get']

        hostaway.api_access_token = cls.auth.access_token
        sdk = hostaway.HostawaySdk(
            id='34661',
            expires_in=cls.auth.expires_in,
            token_type=cls.auth.token_type,
            scope=cls.auth.scope,
        )
        response = sdk.get_calendar(
            '87992',  # Listing ID
            start_date,
            end_date
        )
        cls.assertEqual(
            len(response.to_dict()['days']), 
            len(cls.json_fixtures['calendar']['get']['result'])
        )
        print('TESTED: get_calendar')

    # TEST: get_reservations
    @patch('hostaway.client.requests.get')
    def test_hostaway_get_reservations(cls, mock_get):
        print('should get reservations')

        start_date = datetime.now()
        end_date = start_date + timedelta(days=365)

        start_date = datetime.strftime(start_date, '%Y-%m-%d')
        end_date = datetime.strftime(end_date, '%Y-%m-%d')

        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['reservations']['get']

        hostaway.api_access_token = cls.auth.access_token
        sdk = hostaway.HostawaySdk(
            id=cls.json_fixtures['api']['id'],
            expires_in=cls.auth.expires_in,
            token_type=cls.auth.token_type,
            scope=cls.auth.scope,
        )
        
        response = sdk.get_reservations(
            '87992',  # Listing ID
            start_date,
            end_date
        )
        cls.assertNotEqual(len(response), 0)
        print('TESTED: get_reservations')

    # TEST: get_reservations (empty)
    @patch('hostaway.client.requests.get')
    def test_hostaway_get_reservations_empty(cls, mock_get):
        print('should get reservations empty')

        start_date = datetime.now()
        end_date = start_date + timedelta(days=365)

        start_date = datetime.strftime(start_date, '%Y-%m-%d')
        end_date = datetime.strftime(end_date, '%Y-%m-%d')

        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['reservations']['empty']

        hostaway.api_access_token = cls.auth.access_token
        sdk = hostaway.HostawaySdk(
            id=cls.json_fixtures['api']['id'],
            expires_in=cls.auth.expires_in,
            token_type=cls.auth.token_type,
            scope=cls.auth.scope,
        )
        
        response = sdk.get_reservations(
            '87992',  # Listing ID
            start_date,
            end_date
        )
        cls.assertEqual(len(response), 0)
        print('TESTED: get_reservations (empty)')

    # TEST: update_calendar
    @patch('hostaway.client.requests.put')
    def test_hostaway_update_calendar(cls, mock_put):
        print('should update calendars by array')
        
        batch_array = [{
                'account_id': '34661',
                'id': 47657967,
                'isAvailable': 1,
                'date': '2022-07-29',
                'price': 100,
                'status': 'available',
            },
            {
                'account_id': '34661',
                'id': 47657968,
                'isAvailable': 1,
                'date': '2022-07-30',
                'price': 100,
                'status': 'available',
        }]

        mock_put.return_value = Mock(status_code=200)
        mock_put.return_value.json.return_value = cls.json_fixtures['calendar']['put']

        hostaway.api_access_token = cls.auth.access_token
        sdk = hostaway.HostawaySdk(
            id=cls.json_fixtures['api']['id'],
            expires_in=cls.auth.expires_in,
            token_type=cls.auth.token_type,
            scope=cls.auth.scope,
        )

        response = sdk.update_calendar(
            '87992',
            batch_array,
        )
        cls.assertNotEqual(response, 200)
        print('TESTED: update_calendar')