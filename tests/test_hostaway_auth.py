#!/usr/bin/env python
# coding: utf-8

from testing_config import BaseTestConfig
from unittest.mock import Mock, patch

from datetime import datetime, timedelta

import hostaway
from hostaway.resource.base import Auth

class TestHostawaySDK(BaseTestConfig):
    """TestHostawaySDK."""

    @patch('hostaway.client.requests.post')
    def test_hostaway_auth_fail_invalid(cls, mock_post):
        print('should auth fail')

        mock_post.return_value = Mock(status_code=401)
        mock_post.return_value.json.return_value = cls.json_fixtures['invalidCredentials']

        try:
            cls.auth = hostaway.Auth.get_token(
                client_id=cls.json_fixtures['api']['id'],
                client_secret='XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            )
        except Exception as e:
            cls.assertEqual(str(e), cls.json_fixtures['invalidCredentials']['message'])

    # @patch('hostaway.client.requests.post')
    def test_hostaway_auth_fail_missing(cls):
        print('should auth missing')

        try:
            cls.auth = hostaway.Auth.get_token(
                client_id=cls.json_fixtures['api']['id'],
                # client_secret=None,
            )
        except Exception as e:
            cls.assertEqual(str(e), 'Invalid Authentication Parameters (ID & Secret)')