# Hostaway Python library

[![CircleCI](https://circleci.com/bb/Harpangell/hostaway-python/tree/master.svg?style=svg&circle-token=3e47a0118e8b37d59b9dae0d884468d3f8f94c99)](https://circleci.com/bb/Harpangell/hostaway-python/tree/master)

## Installation

```bash
pip install git+git://bitbucket.org/Harpangell/hostaway-python.git@master
```

To install from source, run

```bash
git clone https://Harpangell@bitbucket.org/Harpangell/hostaway-python.git
cd hostaway
python setup.py install
```

## Documentation

Please see the [Hostaway API Documentation](https://api-co.hostaway.com/Documentation/#getting-started) for the most up-to-date API documentation.

### Usage

This library has only been tested using Python 3.6.?.

Getting and interacting with accounts:

```python
import hostaway

hostaway.api_key = 'xxx'
hostaway.env = 'production'
hostaway.api_client_id = 'xxx'
hostaway.api_client_secret = 'xxx'
hostaway.api_version = 'v2'

client = hostaway.Account()

properties = client.properties
propertyName = properties[0].name
print(propertyName)
```

Properties are cached on each model instance. To refresh, do `property = client.get_property(propertyID)`. (TODO: allow properties to be refreshed manually)

Objects embedded in API responses are added as properties on each model instance. To refresh, do `properties.refresh()`.

Interacting with calendar:

```python
listing_hash = '1'
new_batch_array = [
  {
    'id': '123456',
  }
]

calendar_result = account.update_calendar(
    listing_hash=listing_hash,
    batch_array=new_batch_array,
)
print calendar_result
```

## Development

We use virtualenv. Install with `[sudo] pip install virtualenv`, initialize with `virtualenv venv`, and activate with `source venv/bin/activate`.

Install the development requirements with `pip install -r requirements/dev.txt`

### Testing

To run the test suite, run `py.test` from the project root.

### Linting

We enforce linting on the code with flake8. Run with `flake8 hostaway` from the project root.

### TODOs

- Testing
- Update Docs
- Fixup Caching
