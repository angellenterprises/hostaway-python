"""mock.py."""
import os
import random
import uuid
from datetime import datetime, timedelta
from hostaway.util import read_json, write_json


def get_month_start(year=None, month=None):
    """get_month_start."""
    return datetime.strptime('{}-{}-{}'.format(year, month, 1), '%Y-%m-%d')


def get_month_end(dt=None):
    """get_month_end."""
    next_dt = dt.replace(day=28) + timedelta(days=4)  # this will never fail
    return next_dt - timedelta(days=next_dt.day)


def backwards_month(month=0):
    """backwards_month."""
    if month == 0:
        return 12
    if month == -1:
        return 11
    if month == -2:
        return 10
    if month == -3:
        return 9
    if month == -4:
        return 8
    if month == -5:
        return 7
    if month == -6:
        return 6
    if month == -7:
        return 5
    if month == -8:
        return 4
    if month == -9:
        return 3
    if month == -10:
        return 2
    if month == -11:
        return 1


def dates_bwn_twodates(start_date, end_date):
    """dates_bwn_twodates."""
    for n in range(int((end_date - start_date).days) + 1):
        yield start_date + timedelta(n)


def to_dt(string):
    """to_dt."""
    return datetime.strptime(string, "%Y-%m-%d")


def to_dtl(string):
    """to_dtl."""
    return datetime.strptime(string, "%Y-%m-%d %H:%M:%S")


def from_dt(date):
    """from_dt."""
    return datetime.strftime(date, "%Y-%m-%d")


def from_dtl(date):
    """from_dtl."""
    return datetime.strftime(date, "%Y-%m-%d %H:%M:%S")


def random_blocks(selected):
    """random_blocks."""
    return 'manual'


def random_price():
    """random_price."""
    return random.randint(200, 250)


def parse_status(selected):
    """random_price."""
    if selected == 'new':
        return 'unavailable'
    if selected == 'modified':
        return 'unavailable'
    if selected == 'ownerStay':
        return 'unavailable'
    return 'available'


def init_mock_calendar(id=None, params=None):
    """random_price."""
    start_at = datetime.now()
    end_at = datetime.now()
    if not params:
        raise ValueError('Invalid Start / End Date')

    start_at = to_dt(params['startDate'])
    end_at = to_dt(params['endDate'])

    day_list = []
    for date in dates_bwn_twodates(start_at, end_at):
        types = ['new', 'modified', 'cancelled', 'ownerStay']
        selected = types[random.randint(0, len(types) - 1)]
        day = {
            'id': '1',
            'date': from_dt(date),
            'listingId': '1',
            'price': random_price(),
            'isAvailable': True,
            'status': parse_status(selected),
        }
        day_list.append(day)
    return day_list


def get_mock_calendar(id=None, params=None):
    """get_mock_calendar."""
    path = os.path.abspath(
        'services/hostaway/mocks/calendar/{}.json'.format(id)
    )
    json_data = read_json(path)
    if not json_data:
        initial_calendar = init_mock_calendar(id=id, params=params)
        write_json(initial_calendar, path)
        return initial_calendar
    return json_data


def get_random_channel():
    """get_random_channel."""
    platforms = ['airbnb', 'homeaway', 'other']
    return platforms[random.randint(0, len(platforms)-1)]


def get_random_status():
    """get_random_status."""
    statuss = ['confirmed', 'unconfirmed']
    return statuss[random.randint(0, len(statuss)-1)]


def get_random_checkin():
    """get_random_checkin."""
    check_in = datetime.now()
    return check_in + timedelta(days=random.randint(1, 21))


def get_random_fare(nights):
    """get_random_fare."""
    return random.randint(99, 400) * nights


def get_day_spread(res_days=None):
    """get_day_spread."""
    full_res_days = []
    for res in res_days:
        if 'arrivalDate' not in res or 'departureDate' not in res:
            raise ValueError('Invalid Datetime Object in Day Spread Days')

        rounded_start = res['arrivalDate'].replace(
            hour=0,
            minute=1,
            second=0,
            microsecond=0
        )
        rounded_end = res['departureDate'].replace(
            hour=0,
            minute=1,
            second=0,
            microsecond=0
        )
        number_days = (rounded_end - rounded_start).days
        for i in range(number_days + 1):
            new_start = rounded_start + timedelta(days=i)
            full_res_days.append(datetime.strftime(new_start, "%Y-%m-%d"))
            i += 1
    return full_res_days


def init_mock_reservations(
    no_list=[],
    id=None,
    start_date=None,
    end_date=None
):
    """init_mock_reservations."""
    res_list = []
    num_reservations = random.randint(0, 3)
    for check_in in range(num_reservations):
        print('GENERATE {} RESERVATIONS'.format(num_reservations))
        nights = random.randint(2, 7)
        check_in = get_random_checkin()
        check_out = check_in + timedelta(days=nights)

        # NO DEPLICATES
        res_spread = get_day_spread([{
            'arrivalDate': check_in,
            'departureDate': check_out - timedelta(days=1),
        }])
        if check_in in no_list:
            print('DUPLICATE CHECKIN ATTEMPT')
            num_reservations + 1
            continue

        for res_day in res_spread:
            if res_day in no_list:
                print('DUPLICATE RES ATTEMPT')
                num_reservations + 1
                continue

        id = str(uuid.uuid4())
        code = str(uuid.uuid4())
        guest = str(uuid.uuid4())
        reservation = {
            'id': random.randint(0, 999999),
            'listingMapId': random.randint(0, 999999),
            'channelId': random.randint(0, 999999),
            'channelName': get_random_channel(),
            'reservationId': id,
            'hostawayReservationId': random.randint(0, 999999),
            'channelReservationId': guest,
            "arrivalDate": from_dt(check_in),
            "departureDate": from_dt(check_out),
            'reservationDate': from_dtl(datetime.now()),
            'nights': nights,
            'confirmationCode': ''.join(
                x.strip() for x in code.split('-')
            )[:6],
            'status': 'new',
            'currency': 'USD',
            'totalPrice': get_random_fare(nights),
            'cleaningFee': 125,
        }
        res_list.append(reservation)
    return res_list


def get_mock_reservations(id=None, start_date=None, end_date=None):
    """get_mock_reservations."""
    path = os.path.abspath(
        'services/hostaway/mocks/reservations/{}.json'.format(id)
    )
    json_data = read_json(path)
    if json_data == []:
        initial_reservations = init_mock_reservations(
            id=id,
            start_date=start_date,
            end_date=end_date
        )
        write_json(initial_reservations, path)
        return initial_reservations

    res_days = []
    for reservation in json_data:
        res_dict = {
            'arrivalDate': to_dt(reservation['arrivalDate']),
            'departureDate': to_dt(reservation['departureDate']) - timedelta(days=1),
        }
        res_days.append(res_dict)

    no_list = get_day_spread(res_days)
    if len(no_list) > 20:
        return json_data

    new_reservations = json_data + init_mock_reservations(
        id=id,
        no_list=no_list,
        start_date=start_date,
        end_date=end_date
    )
    write_json(new_reservations, path)
    return new_reservations


def get_mock_listings():
    """get_mock_listings."""
    return [
        {
            "id": "1",
            "active": True,
            "personCapacity": 12,
            "street": "7181 Taco Ave",
            "city": "Newport Beach",
            "state": "California",
            "zipcode": "92663",
            "country": "United States",
            "lat": 0,
            "lng": 0,
            "bathroomsNumber": 3,
            "bedroomsNumber": 4,
            "bedsNumber": 8,
            "checkInTimeStart": "17:00",
            "checkOutTime": "10:00",
            # "integrations": integrationCollection,
            "internalListingName": "7181 Taco Ave",
            "thumbnailUrl": 'https://www.standout-cabin-designs.com/images/storybook-cottages3-8.jpg',
            "cleaningFee": 200,
            "price": 300,
            "currencyCode": "USD",
        },
        {
            "id": "2",
            "active": True,
            "personCapacity": 7,
            "street": "2116 Thomas Avenue",
            "city": "Newport Beach",
            "state": "California",
            "zipcode": "92663",
            "country": "United States",
            "lat": 0,
            "lng": 0,
            "bathroomsNumber": 1.5,
            "bedroomsNumber": 3,
            "bedsNumber": 4,
            "checkInTimeStart": "17:00",
            "checkOutTime": "10:00",
            # "integrations": integrationCollection,
            "internalListingName": "2116 Thomas Avenue",
            "thumbnailUrl": 'https://www.topsiderhomes.com/images/lhouse.jpg',
            "cleaningFee": 100,
            "price": 148,
            "currencyCode": "USD",
        },
        {
            "id": "3",
            "active": True,
            "personCapacity": 4,
            "street": "3957 Louis Avenue",
            "city": "Newport Beach",
            "state": "California",
            "zipcode": "92663",
            "country": "United States",
            "lat": 0,
            "lng": 0,
            "bathroomsNumber": 1,
            "bedroomsNumber": 2,
            "bedsNumber": 2,
            "checkInTimeStart": "17:00",
            "checkOutTime": "10:00",
            # "integrations": integrationCollection,
            "internalListingName": "3957 Louis Avenue",
            "thumbnailUrl": 'https://cambusbarron.com/about/images/greirsonhouse.jpg',
            "cleaningFee": 75,
            "price": 150,
            "currencyCode": "USD",
        },
        {
            "id": "4",
            "active": True,
            "personCapacity": 16,
            "street": "3670 Wyoming St",
            "city": "Newport Beach",
            "state": "California",
            "zipcode": "92663",
            "country": "United States",
            "lat": 0,
            "lng": 0,
            "bathroomsNumber": 3,
            "bedroomsNumber": 7,
            "bedsNumber": 11,
            "checkInTimeStart": "17:00",
            "checkOutTime": "10:00",
            # "integrations": integrationCollection,
            "internalListingName": "3670 Wyoming St",
            "thumbnailUrl": 'https://kids.kiddle.co/images/thumb/6/6c/Nathaniel_Jenks_House_Pawtucket_RI.jpg/183px-Nathaniel_Jenks_House_Pawtucket_RI.jpg',
            "cleaningFee": 75,
            "price": 150,
            "currencyCode": "USD",
        },
        {
            "id": "5",
            "active": True,
            "personCapacity": 6,
            "street": "2310 Ash Lane",
            "city": "Newport Beach",
            "state": "California",
            "zipcode": "92663",
            "country": "United States",
            "lat": 0,
            "lng": 0,
            "bathroomsNumber": 1.5,
            "bedroomsNumber": 3,
            "bedsNumber": 4,
            "checkInTimeStart": "17:00",
            "checkOutTime": "10:00",
            # "integrations": integrationCollection,
            "internalListingName": "2310 Ash Lane",
            "thumbnailUrl": 'https://photos2.fotosearch.com/bthumb/CSP/CSP266/doghouse-clipart__k22315172.jpg',
            "cleaningFee": 100,
            "price": 130,
            "currencyCode": "USD",
        }
    ]
