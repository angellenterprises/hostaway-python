class hostawayError(Exception):
    def __init__(self, error, status_code=None, headers=None):
        super(hostawayError, self).__init__(error)

        self.error = error
        self.status_code = status_code
        self.headers = headers

    def __unicode__(self):
        return self.error


class APIError(hostawayError):
    pass


class APIConnectionError(hostawayError):
    pass


class InvalidRequestError(hostawayError):
    pass


class AuthenticationError(hostawayError):
    pass
