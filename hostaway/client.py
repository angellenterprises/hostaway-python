from __future__ import unicode_literals

import requests
import textwrap
import json
from base64 import b64encode

from hostaway import api_base, error


def build_url(endpoint):
    url = api_base + '/'

    if endpoint:
        url += endpoint

    return url


def get_env():
    from hostaway import env

    if not env:
        raise error.AuthenticationError(
            'No ENV provided. (HINT: set your ENV using '
            '"hostaway.env = <DBRef>"). '
        )

    return env

def get_auth_headers():
    return {
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }


def revoke_token(auth_token=None):

    if not auth_token:
        raise error.AuthenticationError(
            'No Auth token provided. (HINT: set your Auth token using '
            '"hostaway.auth_token = <AUTH-TOKEN>"). You can generate Auth Tokens keys '
            'from the hostaway web interface.'
        )

    params = {
        'token': auth_token,
    }
    res = requests.delete(api_base, params=params, headers=get_auth_headers())
    data = json.loads(res.text)
    return 200
    # return data['login']['id']


def get_headers():
    from hostaway import api_access_token

    if not api_access_token:
        raise error.AuthenticationError(
            'No Auth token provided. (HINT: set your Auth token using '
            '"hostaway.api_access_token = <AUTH-TOKEN>"). You can generate Auth Tokens keys '
            'from the hostaway web interface.'
        )
    return {
        'authorization': 'Bearer {}'.format(api_access_token),
        'accept': 'application/json',
        'content-type': 'application/json',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }


def get(url, params):
    try:
        params['provider'] = 'nightpricer'
        res = requests.get(url, params=params, headers=get_headers())
    except Exception as e:
        handle_request_error(e)

    return handle_response(res)


def put(url, json):
    try:
        res = requests.put(url, params={'provider': 'nightpricer'}, json=json, headers=get_headers())
    except Exception as e:
        handle_request_error(e)

    return res.text


def delete(url):
    try:
        res = requests.delete(url, params={'provider': 'nightpricer'}, headers=get_headers())
    except Exception as e:
        handle_request_error(e)

    return handle_response(res)


def post(url, json):
    try:
        res = requests.post(url, params={'provider': 'nightpricer'}, data=json, headers=get_auth_headers())
    except Exception as e:
        handle_request_error(e)

    return handle_response(res)


def handle_response(res):
    try:
        # res.raise_for_status()
        json = res.json()
    except ValueError as e:
        handle_parse_error(e)

    if not (200 <= res.status_code < 300):
        handle_error_code(json, res.status_code, res.headers)

    return json


def handle_request_error(e):
    if isinstance(e, requests.exceptions.RequestException):
        msg = 'Unexpected error communicating with Hostaway.'
        err = '{}: {}'.format(type(e).__name__, str(e))
    else:
        msg = ('Unexpected error communicating with Hostaway. '
               'It looks like there\'s probably a configuration '
               'issue locally.')
        err = 'A {} was raised'.format(type(e).__name__)
        if u'%s' % e:
            err += ' with error message {}'.format(e)
        else:
            err += ' with no error message'

    msg = textwrap.fill(msg) + '\n\n(Network error: {})'.format(err)
    raise error.APIConnectionError(msg)


def handle_error_code(json, status_code, headers):
    if status_code == 400:
        err = json.get('message', 'Bad request')
        raise error.InvalidRequestError(err, status_code, headers)
    elif status_code == 401:
        err = json.get('message', 'Not authorized')
        raise error.AuthenticationError(err, status_code, headers)
    elif status_code == 404:
        err = json.get('message', 'Not found')
        raise error.InvalidRequestError(err, status_code, headers)
    elif status_code == 500:
        err = json.get('message', 'Internal server error')
        raise error.APIError(err, status_code, headers)
    else:
        err = json.get('message', 'Unknown status code ({})'.format(status_code))
        raise error.APIError(err, status_code, headers)


def handle_parse_error(e, status_code=None, headers=None):
    err = '{}: {}'.format(type(e).__name__, e)
    msg = 'Error parsing hostaway JSON response. \n\n{}'.format(err)
    raise error.APIError(msg, status_code, headers)
