"""__init__."""
api_access_token = None
api_version = 'v1'
env = 'production'
api_base = 'https://api.hostaway.com/{}'.format(api_version)

from hostaway.resource.base import (  # noqa
    Auth,
    HostawaySdk,
)
