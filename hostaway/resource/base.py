#!/usr/bin/env python
# coding: utf-8

import json
import math
from typing import List, Dict
from datetime import datetime

from hostaway import client, error
from hostaway.resource import AuthResource, HostawayResource

from .types import (
    ListingResponse,
    CalendarResponse,
    ReservationResponse,
)

from hostaway.resource.listing import ListingResource
from hostaway.resource.calendar import CalendarResource
from hostaway.resource.reservation import ReservationResource

class Auth(AuthResource):

    def refresh_from(cls, **kwargs):
        cls.access_token = kwargs['access_token']
        cls.expires_in = kwargs['expires_in']
        cls.token_type = kwargs['token_type']
        cls.scope = 'general'

class HostawaySdk(HostawayResource):

    def refresh_from(cls, **kwargs):
        """refresh_from."""
        cls.id = kwargs['id']
        cls.expires_in = kwargs['expires_in']
        cls.token_type = kwargs['token_type']
        cls.scope = kwargs['scope']

    def to_any_object(cls):
        """to_any_object."""
        return {
            'id': cls.id,
            'expires_in': cls.expires_in,
            'token_type': cls.token_type,
            'scope': cls.scope,
        }

    def get_listings(cls) -> List[ListingResponse]:
        """Returns the dict as a model

        :return: The ListingResponse of this ListingResponse.  # noqa: E501
        :rtype: ListingResponse
        """

        new_listings = []
        limit = 10
        params = {
            'limit': 1000,
        }
        res = client.get(ListingResource.list_url(), params)

        # Pagination
        total_count = 0
        if 'count' in res:
            total_count = res['count']
        if 'limit' in res:
            limit = res['limit']
        
        num_pages = math.ceil(total_count / limit)

        # First Results
        res = res['result']
        new_listings.extend([ListingResponse(**l) for l in res])
        
        # More Results
        for page in range(1, num_pages):
            params = {}
            params['skip'] = page * limit
            params['limit'] = limit
            res = client.get(ListingResource.list_url(), params)
            res = res['result']
            new_listings.extend([ListingResponse(**i) for i in res])
        return new_listings

    def get_calendar(
        cls,
        id: str,
        start_date: str,
        end_date: str
    ) -> CalendarResponse:
        """Returns the dict as a model

        :return: The CalendarResponse of this CalendarResponse.  # noqa: E501
        :rtype: CalendarResponse
        """

        if not isinstance(start_date, str):
            raise error.InvalidRequestError('Start Date')

        if not isinstance(end_date, str):
            raise error.InvalidRequestError('End Date')


        params = {
            'startDate': start_date,
            'endDate': end_date,
        }
        res = {}
        resp = client.get(CalendarResource.get_url(id), params)
        res['days'] = resp['result']
        return CalendarResponse(**res)

    def get_reservations(
        cls, 
        id: str, 
        start_date: str, 
        end_date: str
    ) -> List[ReservationResponse]:
        """Returns the dict as a model

        :return: The ReservationResponse of this ReservationResponse.  # noqa: E501
        :rtype: ReservationResponse
        """

        if not isinstance(start_date, str):
            raise error.InvalidRequestError('Start Date')

        if not isinstance(end_date, str):
            raise error.InvalidRequestError('End Date')

        new_reservations = []
        limit = 10
        params = {
            'limit': 1000,
            'listingId': id,
            'arrivalStartDate': start_date,
            'arrivalEndDate': end_date,
        }

        res = client.get(ReservationResource.list_url(), params)

        # Pagination
        total_count = 0
        if 'count' in res:
            total_count = res['count']
        if 'limit' in res:
            limit = res['limit']
        num_pages = math.ceil(total_count / limit)

        # Add First Page Results
        res = res['result']
        new_reservations.extend(ReservationResponse(**r) for r in res)

        # More Results
        for page in range(1, num_pages):
            params['skip'] = page * limit
            params['limit'] = limit
            res = client.get(ReservationResource.list_url(), params)
            res = res['result']
            new_reservations.extend([ReservationResponse(**r) for r in res])

        return new_reservations

    def update_calendar(
        cls,
        id: str,
        batch_array: List[Dict]
    ) -> Dict:
        """Returns the dict as a model

        :return: The dict of this dict.  # noqa: E501
        :rtype: dict
        """

        def get_arrays_from_array(batch_days_array):
            """get_arrays_from_array."""
            updated_calendar_arrays = []
            updated_calendar_days = []
            sbb_batch_max = 200
            for day in batch_days_array:
                updated_calendar_days.append(day)
                if len(updated_calendar_days) == sbb_batch_max:
                    updated_calendar_arrays.append(updated_calendar_days)
                    updated_calendar_days = []

            # Add non 200 batch (last)
            if len(updated_calendar_days) > 0:
                updated_calendar_arrays.append(updated_calendar_days)
            return updated_calendar_arrays


        # def format_array(batch_array):
        #     """format_array."""
        #     new_array = []
        #     for day in batch_array:
        #         day['startDate'] = day['date']
        #         day['endDate'] = day['date']
        #         del day['date']
        #         new_array.append(day)
        #     return new_array

        # clean_batch_array = format_array(batch_array)
        arrays = get_arrays_from_array(batch_array)
        results = []
        for array in arrays:
            res = client.put(CalendarResource.put_url(id), array)
            results.append(res)
        return results

    def __unicode__(cls):
        """__unicode__."""
        return '<{} {}>'.format(cls.__class__.__name__, cls.id)