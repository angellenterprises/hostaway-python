"""calendar."""

from __future__ import unicode_literals
from hostaway import client
from hostaway.resource import HostawayAccountResource
# import json


def get_arrays_from_array(batch_days_array):
    """get_arrays_from_array."""
    updated_calendar_arrays = []
    updated_calendar_days = []
    sbb_batch_max = 200
    for day in batch_days_array:
        updated_calendar_days.append(day)
        if len(updated_calendar_days) == sbb_batch_max:
            updated_calendar_arrays.append(updated_calendar_days)
            updated_calendar_days = []

    # Add non 200 batch (last)
    if len(updated_calendar_days) > 0:
        updated_calendar_arrays.append(updated_calendar_days)
    return updated_calendar_arrays


def format_array(batch_array):
    """format_array."""
    new_array = []
    for day in batch_array:
        day['startDate'] = day['date']
        day['endDate'] = day['date']
        del day['date']
        new_array.append(day)
    return new_array


class Calendar(HostawayAccountResource):
    """Calendar."""

    @classmethod
    def get_url(cls, id):
        """get_url."""
        return super(Calendar, cls).list_url(None) + 'listings/' + id + '/calendar'

    @classmethod
    def put_url(cls, id):
        """put_url."""
        return super(Calendar, cls).list_url(None) + 'listings/' + id + '/calendarIntervals'

    @classmethod
    def update(cls, id, batch_array):
        """update."""
        clean_batch_array = format_array(batch_array)
        arrays = get_arrays_from_array(clean_batch_array)
        results = []
        for array in arrays:
            res = client.put(cls.put_url(id), array)
            results.append(res)
        return results

    def refresh_from(self, **kwargs):
        """refresh_from."""
        days_collection = []
        for day in kwargs['days']:
            days_collection.append(Day(self.account_id, **day))
        self.days = days_collection

    def to_any_object(self):
        """to_any_object."""
        days_collection = []
        for day in self.days:
            days_collection.append(day.to_any_object())
        return {
            'days': days_collection,
        }


class Day(HostawayAccountResource):
    """Day."""

    def refresh_from(self, **kwargs):
        """refresh_from."""
        self.id = kwargs['id']
        self.isAvailable = kwargs['isAvailable']
        self.date = kwargs['date']
        self.price = kwargs['price']
        self.status = kwargs['status']

    def to_any_object(self):
        """to_any_object."""
        return {
            'id': self.id,
            'isAvailable': self.isAvailable,
            'date': self.date,
            'price': self.price,
            'status': self.status,
        }
