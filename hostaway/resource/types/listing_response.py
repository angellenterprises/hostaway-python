#!/usr/bin/env python
# coding: utf-8

from hostaway.resource import HostawayResource
from typing import List, Dict


class ListingResponse(HostawayResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    nullable = {
        'city': True,
        'check_in_time_start': True,
        'check_out_time': True,
        'thumbnail_url': True,
    }

    required = {
        'id': True,
        # 'person_capacity': True,
        # 'city': True,
        # 'street': True,
        # 'state': True,
        # 'zipcode': True,
        # 'country': True,
        # 'lat': True,
        # 'lng': True,
        # 'bathrooms_number': True,
        # 'bedrooms_number': True,
        # 'beds_number': True,
        # 'check_in_time_start': True,
        # 'check_out_time': True,
        'external_listing_name': True,
        # 'thumbnail_url': True,
        # 'cleaning_fee': True,
        'price': True,
        'currency_code': True
    }

    model_types = {
        'id': int,
        'person_capacity': int,
        'city': str,
        'street': str,
        'state': str,
        'zipcode': str,
        'country': str,
        'lat': float,
        'lng': float,
        'bathrooms_number': int,
        'bedrooms_number': int,
        'beds_number': int,
        'check_in_time_start': int,
        'check_out_time': int,
        'external_listing_name': str,
        'thumbnail_url': str,
        'cleaning_fee': float,
        'price': float,
        'currency_code': str
    }

    attribute_map = {
        'id': 'id',
        'person_capacity': 'personCapacity',
        'city': 'city',
        'street': 'street',
        'state': 'state',
        'zipcode': 'zipcode',
        'country': 'country',
        'lat': 'lat',
        'lng': 'lng',
        'bathrooms_number': 'bathroomsNumber',
        'bedrooms_number': 'bedroomsNumber',
        'beds_number': 'bedsNumber',
        'check_in_time_start': 'checkInTimeStart',
        'check_out_time': 'checkOutTime',
        'external_listing_name': 'externalListingName',
        'thumbnail_url': 'thumbnailUrl',
        'cleaning_fee': 'cleaningFee',
        'price': 'price',
        'currency_code': 'currencyCode'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)
        cls._id = None
        cls._person_capacity = None
        cls._city = None
        cls._street = None
        cls._state = None
        cls._zipcode = None
        cls._country = None
        cls._lat = None
        cls._lng = None
        cls._bathrooms_number = None
        cls._bedrooms_number = None
        cls._beds_number = None
        cls._check_in_time_start = None
        cls._check_out_time = None
        cls._external_listing_name = None
        cls._thumbnail_url = None
        cls._cleaning_fee = None
        cls._price = None
        cls._currency_code = None

        cls.id = kwargs['id']
        cls.person_capacity = kwargs['personCapacity']
        cls.city = kwargs['city']
        cls.street = kwargs['street']
        cls.state = kwargs['state']
        cls.zipcode = kwargs['zipcode']
        cls.country = kwargs['country']
        cls.lat = kwargs['lat']
        cls.lng = kwargs['lng']
        cls.bathrooms_number = kwargs['bathroomsNumber']
        cls.bedrooms_number = kwargs['bedroomsNumber']
        cls.beds_number = kwargs['bedsNumber']
        cls.check_in_time_start = kwargs['checkInTimeStart']
        cls.check_out_time = kwargs['checkOutTime']
        cls.external_listing_name = kwargs['externalListingName']
        cls.thumbnail_url = kwargs['thumbnailUrl']
        cls.cleaning_fee = kwargs['cleaningFee']
        cls.price = kwargs['price']
        cls.currency_code = kwargs['currencyCode']

    @property
    def id(cls) -> int:
        """Gets the id of this ListingResponse.


        :return: The id of this ListingResponse.
        :rtype: int
        """
        return cls._id

    @id.setter
    def id(cls, id: int):
        """Sets the id of this ListingResponse.


        :param id: The id of this ListingResponse.
        :type id: int
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        cls._id = id

    @property
    def person_capacity(cls) -> int:
        """Gets the person_capacity of this ListingResponse.


        :return: The person_capacity of this ListingResponse.
        :rtype: int
        """
        return cls._person_capacity

    @person_capacity.setter
    def person_capacity(cls, person_capacity: int):
        """Sets the person_capacity of this ListingResponse.


        :param person_capacity: The person_capacity of this ListingResponse.
        :type person_capacity: int
        """
        # if person_capacity is None:
        #     raise ValueError("Invalid value for `person_capacity`, must not be `None`")  # noqa: E501

        cls._person_capacity = person_capacity

    @property
    def city(cls) -> str:
        """Gets the city of this ListingResponse.


        :return: The city of this ListingResponse.
        :rtype: str
        """
        return cls._city

    @city.setter
    def city(cls, city: str):
        """Sets the city of this ListingResponse.


        :param city: The city of this ListingResponse.
        :type city: str
        """
        # if city is None:
        #     raise ValueError("Invalid value for `city`, must not be `None`")  # noqa: E501

        cls._city = city

    @property
    def street(cls) -> str:
        """Gets the street of this ListingResponse.


        :return: The street of this ListingResponse.
        :rtype: str
        """
        return cls._street

    @street.setter
    def street(cls, street: str):
        """Sets the street of this ListingResponse.


        :param street: The street of this ListingResponse.
        :type street: str
        """
        # if street is None:
        #     raise ValueError("Invalid value for `street`, must not be `None`")  # noqa: E501

        cls._street = street

    @property
    def state(cls) -> str:
        """Gets the state of this ListingResponse.


        :return: The state of this ListingResponse.
        :rtype: str
        """
        return cls._state

    @state.setter
    def state(cls, state: str):
        """Sets the state of this ListingResponse.


        :param state: The state of this ListingResponse.
        :type state: str
        """
        # if state is None:
        #     raise ValueError("Invalid value for `state`, must not be `None`")  # noqa: E501

        cls._state = state

    @property
    def zipcode(cls) -> str:
        """Gets the zipcode of this ListingResponse.


        :return: The zipcode of this ListingResponse.
        :rtype: str
        """
        return cls._zipcode

    @zipcode.setter
    def zipcode(cls, zipcode: str):
        """Sets the zipcode of this ListingResponse.


        :param zipcode: The zipcode of this ListingResponse.
        :type zipcode: str
        """
        # if zipcode is None:
        #     raise ValueError("Invalid value for `zipcode`, must not be `None`")  # noqa: E501

        cls._zipcode = zipcode

    @property
    def country(cls) -> str:
        """Gets the country of this ListingResponse.


        :return: The country of this ListingResponse.
        :rtype: str
        """
        return cls._country

    @country.setter
    def country(cls, country: str):
        """Sets the country of this ListingResponse.


        :param country: The country of this ListingResponse.
        :type country: str
        """
        # if country is None:
        #     raise ValueError("Invalid value for `country`, must not be `None`")  # noqa: E501

        cls._country = country

    @property
    def lat(cls) -> float:
        """Gets the lat of this ListingResponse.


        :return: The lat of this ListingResponse.
        :rtype: float
        """
        return cls._lat

    @lat.setter
    def lat(cls, lat: float):
        """Sets the lat of this ListingResponse.


        :param lat: The lat of this ListingResponse.
        :type lat: float
        """
        # if lat is None:
        #     raise ValueError("Invalid value for `lat`, must not be `None`")  # noqa: E501

        cls._lat = lat

    @property
    def lng(cls) -> float:
        """Gets the lng of this ListingResponse.


        :return: The lng of this ListingResponse.
        :rtype: float
        """
        return cls._lng

    @lng.setter
    def lng(cls, lng: float):
        """Sets the lng of this ListingResponse.


        :param lng: The lng of this ListingResponse.
        :type lng: float
        """
        # if lng is None:
        #     raise ValueError("Invalid value for `lng`, must not be `None`")  # noqa: E501

        cls._lng = lng

    @property
    def bathrooms_number(cls) -> int:
        """Gets the bathrooms_number of this ListingResponse.


        :return: The bathrooms_number of this ListingResponse.
        :rtype: int
        """
        return cls._bathrooms_number

    @bathrooms_number.setter
    def bathrooms_number(cls, bathrooms_number: int):
        """Sets the bathrooms_number of this ListingResponse.


        :param bathrooms_number: The bathrooms_number of this ListingResponse.
        :type bathrooms_number: int
        """
        # if bathrooms_number is None:
        #     raise ValueError("Invalid value for `bathrooms_number`, must not be `None`")  # noqa: E501

        cls._bathrooms_number = bathrooms_number

    @property
    def bedrooms_number(cls) -> int:
        """Gets the bedrooms_number of this ListingResponse.


        :return: The bedrooms_number of this ListingResponse.
        :rtype: int
        """
        return cls._bedrooms_number

    @bedrooms_number.setter
    def bedrooms_number(cls, bedrooms_number: int):
        """Sets the bedrooms_number of this ListingResponse.


        :param bedrooms_number: The bedrooms_number of this ListingResponse.
        :type bedrooms_number: int
        """
        # if bedrooms_number is None:
        #     raise ValueError("Invalid value for `bedrooms_number`, must not be `None`")  # noqa: E501

        cls._bedrooms_number = bedrooms_number

    @property
    def beds_number(cls) -> int:
        """Gets the beds_number of this ListingResponse.


        :return: The beds_number of this ListingResponse.
        :rtype: int
        """
        return cls._beds_number

    @beds_number.setter
    def beds_number(cls, beds_number: int):
        """Sets the beds_number of this ListingResponse.


        :param beds_number: The beds_number of this ListingResponse.
        :type beds_number: int
        """
        # if beds_number is None:
        #     raise ValueError("Invalid value for `beds_number`, must not be `None`")  # noqa: E501

        cls._beds_number = beds_number

    @property
    def check_in_time_start(cls) -> int:
        """Gets the check_in_time_start of this ListingResponse.


        :return: The check_in_time_start of this ListingResponse.
        :rtype: int
        """
        return cls._check_in_time_start

    @check_in_time_start.setter
    def check_in_time_start(cls, check_in_time_start: int):
        """Sets the check_in_time_start of this ListingResponse.


        :param check_in_time_start: The check_in_time_start of this ListingResponse.
        :type check_in_time_start: int
        """
        # if check_in_time_start is None:
        #     raise ValueError("Invalid value for `check_in_time_start`, must not be `None`")  # noqa: E501

        cls._check_in_time_start = check_in_time_start

    @property
    def check_out_time(cls) -> int:
        """Gets the check_out_time of this ListingResponse.


        :return: The check_out_time of this ListingResponse.
        :rtype: int
        """
        return cls._check_out_time

    @check_out_time.setter
    def check_out_time(cls, check_out_time: int):
        """Sets the check_out_time of this ListingResponse.


        :param check_out_time: The check_out_time of this ListingResponse.
        :type check_out_time: int
        """
        # if check_out_time is None:
        #     raise ValueError("Invalid value for `check_out_time`, must not be `None`")  # noqa: E501

        cls._check_out_time = check_out_time

    @property
    def external_listing_name(cls) -> str:
        """Gets the external_listing_name of this ListingResponse.


        :return: The external_listing_name of this ListingResponse.
        :rtype: str
        """
        return cls._external_listing_name

    @external_listing_name.setter
    def external_listing_name(cls, external_listing_name: str):
        """Sets the external_listing_name of this ListingResponse.


        :param external_listing_name: The external_listing_name of this ListingResponse.
        :type external_listing_name: str
        """
        if external_listing_name is None:
            raise ValueError("Invalid value for `external_listing_name`, must not be `None`")  # noqa: E501

        cls._external_listing_name = external_listing_name

    @property
    def thumbnail_url(cls) -> str:
        """Gets the thumbnail_url of this ListingResponse.


        :return: The thumbnail_url of this ListingResponse.
        :rtype: str
        """
        return cls._thumbnail_url

    @thumbnail_url.setter
    def thumbnail_url(cls, thumbnail_url: str):
        """Sets the thumbnail_url of this ListingResponse.


        :param thumbnail_url: The thumbnail_url of this ListingResponse.
        :type thumbnail_url: str
        """
        # if thumbnail_url is None:
        #     raise ValueError("Invalid value for `thumbnail_url`, must not be `None`")  # noqa: E501

        cls._thumbnail_url = thumbnail_url

    @property
    def cleaning_fee(cls) -> float:
        """Gets the cleaning_fee of this ListingResponse.


        :return: The cleaning_fee of this ListingResponse.
        :rtype: float
        """
        return cls._cleaning_fee

    @cleaning_fee.setter
    def cleaning_fee(cls, cleaning_fee: float):
        """Sets the cleaning_fee of this ListingResponse.


        :param cleaning_fee: The cleaning_fee of this ListingResponse.
        :type cleaning_fee: float
        """
        # if cleaning_fee is None:
        #     raise ValueError("Invalid value for `cleaning_fee`, must not be `None`")  # noqa: E501

        cls._cleaning_fee = cleaning_fee

    @property
    def price(cls) -> float:
        """Gets the price of this ListingResponse.


        :return: The price of this ListingResponse.
        :rtype: float
        """
        return cls._price

    @price.setter
    def price(cls, price: float):
        """Sets the price of this ListingResponse.


        :param price: The price of this ListingResponse.
        :type price: float
        """
        if price is None:
            raise ValueError("Invalid value for `price`, must not be `None`")  # noqa: E501

        cls._price = price

    @property
    def currency_code(cls) -> str:
        """Gets the currency_code of this ListingResponse.


        :return: The currency_code of this ListingResponse.
        :rtype: str
        """
        return cls._currency_code

    @currency_code.setter
    def currency_code(cls, currency_code: str):
        """Sets the currency_code of this ListingResponse.


        :param currency_code: The currency_code of this ListingResponse.
        :type currency_code: str
        """
        if currency_code is None:
            raise ValueError("Invalid value for `currency_code`, must not be `None`")  # noqa: E501

        cls._currency_code = currency_code
