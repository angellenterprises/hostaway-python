#!/usr/bin/env python
# coding: utf-8

from hostaway.resource import HostawayResource
from typing import List


class Day(HostawayResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'id': True,
        # 'is_available': True,
        '_date': True,
        # 'price': True,
        # 'status': True
    }

    model_types = {
        'id': int,
        'is_available': int,
        '_date': str,
        'price': float,
        'status': str
    }

    attribute_map = {
        'id': 'id',
        'is_available': 'isAvailable',
        '_date': 'date',
        'price': 'price',
        'status': 'status'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Day of this Day.  # noqa: E501
        :rtype: Day
        """
        print(kwargs)
        cls.sanity_check(kwargs)

        cls._id = None
        cls._is_available = None
        cls.__date = None
        cls._price = None
        cls._status = None

        cls.id = kwargs['id']
        cls.is_available = kwargs['isAvailable']
        cls._date = kwargs['date']
        cls.price = kwargs['price']
        cls.status = kwargs['status']

    @property
    def id(cls) -> int:
        """Gets the id of this Day.


        :return: The id of this Day.
        :rtype: int
        """
        return cls._id

    @id.setter
    def id(cls, id: int):
        """Sets the id of this Day.


        :param id: The id of this Day.
        :type id: int
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        cls._id = id

    @property
    def is_available(cls) -> int:
        """Gets the is_available of this Day.


        :return: The is_available of this Day.
        :rtype: int
        """
        return cls._is_available

    @is_available.setter
    def is_available(cls, is_available: int):
        """Sets the is_available of this Day.


        :param is_available: The is_available of this Day.
        :type is_available: int
        """
        # if is_available is None:
        #     raise ValueError("Invalid value for `is_available`, must not be `None`")  # noqa: E501

        cls._is_available = is_available

    @property
    def _date(cls) -> str:
        """Gets the _date of this Day.


        :return: The _date of this Day.
        :rtype: str
        """
        return cls.__date

    @_date.setter
    def _date(cls, _date: str):
        """Sets the _date of this Day.


        :param _date: The _date of this Day.
        :type _date: str
        """
        if _date is None:
            raise ValueError("Invalid value for `_date`, must not be `None`")  # noqa: E501

        cls.__date = _date

    @property
    def price(cls) -> float:
        """Gets the price of this Day.


        :return: The price of this Day.
        :rtype: float
        """
        return cls._price

    @price.setter
    def price(cls, price: float):
        """Sets the price of this Day.


        :param price: The price of this Day.
        :type price: float
        """
        # if price is None:
        #     raise ValueError("Invalid value for `price`, must not be `None`")  # noqa: E501

        cls._price = price

    @property
    def status(cls) -> str:
        """Gets the status of this Day.


        :return: The status of this Day.
        :rtype: str
        """
        return cls._status

    @status.setter
    def status(cls, status: str):
        """Sets the status of this Day.


        :param status: The status of this Day.
        :type status: str
        """
        # if status is None:
        #     raise ValueError("Invalid value for `status`, must not be `None`")  # noqa: E501

        cls._status = status


class CalendarResponse(HostawayResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'days': True,
    }

    model_types = {
        'days': list,
    }

    attribute_map = {
        'days': 'days',
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The CalendarResponse of this CalendarResponse.  # noqa: E501
        :rtype: CalendarResponse
        """
        cls.sanity_check(kwargs)
        cls._days = None

        cls.days = [Day(**d) for d in kwargs['days']]

    @property
    def days(self) -> List[Day]:
        """Gets the days of this CalendarResponse.


        :return: The days of this CalendarResponse.
        :rtype: List[Day]
        """
        return self._days

    @days.setter
    def days(self, days: List[Day]):
        """Sets the days of this CalendarResponse.


        :param days: The days of this CalendarResponse.
        :type days: List[Day]
        """
        if days is None:
            raise ValueError("Invalid value for `days`, must not be `None`")  # noqa: E501

        self._days = days