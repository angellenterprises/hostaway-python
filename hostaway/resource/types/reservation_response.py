#!/usr/bin/env python
# coding: utf-8

from hostaway.resource import HostawayResource
from typing import List, Dict


class ReservationResponse(HostawayResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """

    nullable = {
        'confirmation_code': True,
    }

    required = {
        'id': True,
        'listing_map_id': True,
        'channel_id': True,
        'reservation_id': True,
        'channel_name': True,
        # 'confirmation_code': True,
        'arrival_date': True,
        'departure_date': True,
        # 'reservation_date': True,
        # 'nights': True,
        'total_price': True,
        # 'currency': True,
        # 'cleaning_fee': True,
        'status': True
    }

    model_types = {
        'id': int,
        'listing_map_id': int,
        'channel_id': int,
        'reservation_id': str,
        'channel_name': str,
        'confirmation_code': str,
        'arrival_date': str,
        'departure_date': str,
        'reservation_date': str,
        'nights': int,
        'total_price': float,
        'currency': str,
        'cleaning_fee': float,
        'status': str
    }

    attribute_map = {
        'id': 'id',
        'listing_map_id': 'listingMapId',
        'channel_id': 'channelId',
        'reservation_id': 'reservationId',
        'channel_name': 'channelName',
        'confirmation_code': 'confirmationCode',
        'arrival_date': 'arrivalDate',
        'departure_date': 'departureDate',
        'reservation_date': 'reservationDate',
        'nights': 'nights',
        'total_price': 'totalPrice',
        'currency': 'currency',
        'cleaning_fee': 'cleaningFee',
        'status': 'status'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)

        cls._id = None
        cls._listing_map_id = None
        cls._channel_id = None
        cls._reservation_id = None
        cls._channel_name = None
        cls._confirmation_code = None
        cls._arrival_date = None
        cls._departure_date = None
        cls._reservation_date = None
        cls._nights = None
        cls._total_price = None
        cls._currency = None
        cls._cleaning_fee = None
        cls._status = None

        cls.id = kwargs['id']
        cls.listing_map_id = kwargs['listingMapId']
        cls.channel_id = kwargs['channelId']
        cls.reservation_id = kwargs['reservationId']
        cls.channel_name = kwargs['channelName']
        if 'confirmationCode' in kwargs:
            cls.confirmation_code = kwargs['confirmationCode']
        cls.arrival_date = kwargs['arrivalDate']
        cls.departure_date = kwargs['departureDate']
        cls.reservation_date = kwargs['reservationDate']
        cls.nights = kwargs['nights']
        cls.total_price = kwargs['totalPrice']
        cls.currency = kwargs['currency']
        cls.cleaning_fee = kwargs['cleaningFee']
        cls.status = kwargs['status']

    @property
    def id(cls) -> int:
        """Gets the id of this ReservationResponse.


        :return: The id of this ReservationResponse.
        :rtype: int
        """
        return cls._id

    @id.setter
    def id(cls, id: int):
        """Sets the id of this ReservationResponse.


        :param id: The id of this ReservationResponse.
        :type id: int
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        cls._id = id

    @property
    def listing_map_id(cls) -> int:
        """Gets the listing_map_id of this ReservationResponse.


        :return: The listing_map_id of this ReservationResponse.
        :rtype: int
        """
        return cls._listing_map_id

    @listing_map_id.setter
    def listing_map_id(cls, listing_map_id: int):
        """Sets the listing_map_id of this ReservationResponse.


        :param listing_map_id: The listing_map_id of this ReservationResponse.
        :type listing_map_id: int
        """
        if listing_map_id is None:
            raise ValueError("Invalid value for `listing_map_id`, must not be `None`")  # noqa: E501

        cls._listing_map_id = listing_map_id

    @property
    def channel_id(cls) -> int:
        """Gets the channel_id of this ReservationResponse.


        :return: The channel_id of this ReservationResponse.
        :rtype: int
        """
        return cls._channel_id

    @channel_id.setter
    def channel_id(cls, channel_id: int):
        """Sets the channel_id of this ReservationResponse.


        :param channel_id: The channel_id of this ReservationResponse.
        :type channel_id: int
        """
        if channel_id is None:
            raise ValueError("Invalid value for `channel_id`, must not be `None`")  # noqa: E501

        cls._channel_id = channel_id

    @property
    def reservation_id(cls) -> str:
        """Gets the reservation_id of this ReservationResponse.


        :return: The reservation_id of this ReservationResponse.
        :rtype: str
        """
        return cls._reservation_id

    @reservation_id.setter
    def reservation_id(cls, reservation_id: str):
        """Sets the reservation_id of this ReservationResponse.


        :param reservation_id: The reservation_id of this ReservationResponse.
        :type reservation_id: str
        """
        if reservation_id is None:
            raise ValueError("Invalid value for `reservation_id`, must not be `None`")  # noqa: E501

        cls._reservation_id = reservation_id

    @property
    def channel_name(cls) -> str:
        """Gets the channel_name of this ReservationResponse.


        :return: The channel_name of this ReservationResponse.
        :rtype: str
        """
        return cls._channel_name

    @channel_name.setter
    def channel_name(cls, channel_name: str):
        """Sets the channel_name of this ReservationResponse.


        :param channel_name: The channel_name of this ReservationResponse.
        :type channel_name: str
        """
        if channel_name is None:
            raise ValueError("Invalid value for `channel_name`, must not be `None`")  # noqa: E501

        cls._channel_name = channel_name

    @property
    def confirmation_code(cls) -> str:
        """Gets the confirmation_code of this ReservationResponse.


        :return: The confirmation_code of this ReservationResponse.
        :rtype: str
        """
        return cls._confirmation_code

    @confirmation_code.setter
    def confirmation_code(cls, confirmation_code: str):
        """Sets the confirmation_code of this ReservationResponse.


        :param confirmation_code: The confirmation_code of this ReservationResponse.
        :type confirmation_code: str
        """
        # if confirmation_code is None:
        #     raise ValueError("Invalid value for `confirmation_code`, must not be `None`")  # noqa: E501

        cls._confirmation_code = confirmation_code

    @property
    def arrival_date(cls) -> str:
        """Gets the arrival_date of this ReservationResponse.


        :return: The arrival_date of this ReservationResponse.
        :rtype: str
        """
        return cls._arrival_date

    @arrival_date.setter
    def arrival_date(cls, arrival_date: str):
        """Sets the arrival_date of this ReservationResponse.


        :param arrival_date: The arrival_date of this ReservationResponse.
        :type arrival_date: str
        """
        if arrival_date is None:
            raise ValueError("Invalid value for `arrival_date`, must not be `None`")  # noqa: E501

        cls._arrival_date = arrival_date

    @property
    def departure_date(cls) -> str:
        """Gets the departure_date of this ReservationResponse.


        :return: The departure_date of this ReservationResponse.
        :rtype: str
        """
        return cls._departure_date

    @departure_date.setter
    def departure_date(cls, departure_date: str):
        """Sets the departure_date of this ReservationResponse.


        :param departure_date: The departure_date of this ReservationResponse.
        :type departure_date: str
        """
        if departure_date is None:
            raise ValueError("Invalid value for `departure_date`, must not be `None`")  # noqa: E501

        cls._departure_date = departure_date

    @property
    def reservation_date(cls) -> str:
        """Gets the reservation_date of this ReservationResponse.


        :return: The reservation_date of this ReservationResponse.
        :rtype: str
        """
        return cls._reservation_date

    @reservation_date.setter
    def reservation_date(cls, reservation_date: str):
        """Sets the reservation_date of this ReservationResponse.


        :param reservation_date: The reservation_date of this ReservationResponse.
        :type reservation_date: str
        """
        # if reservation_date is None:
        #     raise ValueError("Invalid value for `reservation_date`, must not be `None`")  # noqa: E501

        cls._reservation_date = reservation_date

    @property
    def nights(cls) -> int:
        """Gets the nights of this ReservationResponse.


        :return: The nights of this ReservationResponse.
        :rtype: int
        """
        return cls._nights

    @nights.setter
    def nights(cls, nights: int):
        """Sets the nights of this ReservationResponse.


        :param nights: The nights of this ReservationResponse.
        :type nights: int
        """
        # if nights is None:
        #     raise ValueError("Invalid value for `nights`, must not be `None`")  # noqa: E501

        cls._nights = nights

    @property
    def total_price(cls) -> float:
        """Gets the total_price of this ReservationResponse.


        :return: The total_price of this ReservationResponse.
        :rtype: float
        """
        return cls._total_price

    @total_price.setter
    def total_price(cls, total_price: float):
        """Sets the total_price of this ReservationResponse.


        :param total_price: The total_price of this ReservationResponse.
        :type total_price: float
        """
        if total_price is None:
            raise ValueError("Invalid value for `total_price`, must not be `None`")  # noqa: E501

        cls._total_price = total_price

    @property
    def currency(cls) -> str:
        """Gets the currency of this ReservationResponse.


        :return: The currency of this ReservationResponse.
        :rtype: str
        """
        return cls._currency

    @currency.setter
    def currency(cls, currency: str):
        """Sets the currency of this ReservationResponse.


        :param currency: The currency of this ReservationResponse.
        :type currency: str
        """
        # if currency is None:
            # raise ValueError("Invalid value for `currency`, must not be `None`")  # noqa: E501

        cls._currency = currency

    @property
    def cleaning_fee(cls) -> float:
        """Gets the cleaning_fee of this ReservationResponse.


        :return: The cleaning_fee of this ReservationResponse.
        :rtype: float
        """
        return cls._cleaning_fee

    @cleaning_fee.setter
    def cleaning_fee(cls, cleaning_fee: float):
        """Sets the cleaning_fee of this ReservationResponse.


        :param cleaning_fee: The cleaning_fee of this ReservationResponse.
        :type cleaning_fee: float
        """
        # if cleaning_fee is None:
        #     raise ValueError("Invalid value for `cleaning_fee`, must not be `None`")  # noqa: E501

        cls._cleaning_fee = cleaning_fee

    @property
    def status(cls) -> str:
        """Gets the status of this ReservationResponse.


        :return: The status of this ReservationResponse.
        :rtype: str
        """
        return cls._status

    @status.setter
    def status(cls, status: str):
        """Sets the status of this ReservationResponse.


        :param status: The status of this ReservationResponse.
        :type status: str
        """
        if status is None:
            raise ValueError("Invalid value for `status`, must not be `None`")  # noqa: E501

        cls._status = status
