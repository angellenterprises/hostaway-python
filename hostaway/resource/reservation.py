#!/usr/bin/env python
# coding: utf-8

from hostaway.resource import HostawayAccountResource


class ReservationResource(HostawayAccountResource):

    @classmethod
    def list_url(cls) -> str:
        """
        Gets the GET url of this ReservationResource

        :return: The GET url of this ReservationResource.
        :rtype: str
        """
        return super(ReservationResource, cls).list_url() + 'reservations'