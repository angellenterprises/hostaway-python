#!/usr/bin/env python
# coding: utf-8

from hostaway.resource import HostawayAccountResource


class CalendarResource(HostawayAccountResource):

    @classmethod
    def get_url(cls, id: str) -> str:
        """
        Gets the GET url of this CalendarResource

        :return: The GET url of this CalendarResource.
        :rtype: str
        """
        return super(CalendarResource, cls).list_url() + 'listings/' + id + '/calendar'

    @classmethod
    def put_url(cls, id: str) -> str:
        """
        Gets the PUT url of this CalendarResource

        :return: The PUT url of this CalendarResource.
        :rtype: str
        """
        return super(CalendarResource, cls).list_url() + 'listings/' + id + '/calendarIntervals'